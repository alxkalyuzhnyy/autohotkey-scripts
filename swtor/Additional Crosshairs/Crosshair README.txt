SWTOR MouseLook Combat mode 1.0 - created by Wayleran 11/14/2015


To replace the default Crosshair simply back up the Crosshair.png file that is with the
"SWTOR MouseLook Combat Mode 1.0.ahk" file in your SWTOR folder and copy/paste a new one
there and rename it to "Crosshair.png".

REMEMBER TO ALWAYS LEAVE/MAKE BACKUPS OF THE OLD CROSSHAIRS.

Also depending on the Crosshair you may need to adjust the "CHW" (Crosshair width) and 
"CHH" (Crosshair height) offsets in SWTOR MouseLook Combat Mode 1.0.ahk file to center
them to your preferences. Simply open "SWTOR MouseLook Combat Mode 1.0.ahk" with notepad
to edit it. Again remember to make a backup of any files you edit.

example:

CHW := W .... change to .... CHW := W-14 to move the crosshair LEFT by 14 pixels.

or

CHH := H+5 .... change to .... CHH := H+15 to move the crosshair DOWN by 10 more pixels.


In addition, if you prefer to make you own all you need to do is create a new "transparent"
.PNG file and name it Crosshair.png and place it with the main .ahk file.