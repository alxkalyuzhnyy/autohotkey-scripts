;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;; SWTOR MouseLook Combat mode 1.0 - created by Wayleran 11/14/2015 ;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

#NoEnv
#SingleInstance Force
DetectHiddenWindows, on
#IfWinActive ahk_exe swtor.exe


;;;;;;;;;; Variables ;;;;;;;;;;

MouseLook := 0						;DO NOT TOUCH - Sets MouseLook mode off initially.

W   := A_ScreenWidth/2				;Sets cursor's X coordinate. By default it's directly in the center of your screen. I'd recommend not adjusting this. (A_ScreenWidth/2 by default)
H   := A_ScreenHeight/2.5			;Sets cursor's Y coordinate. Change the 2.5 up or down to your preferred cursor height. (A_ScreenHeight/2.5 by default)

CHW := W							;Offsets the Crosshair horizontally (+/- to adjust to your preference). (Just W by default)
CHH := H+5							;Offsets the Crosshair vertically (+/- to adjust to your preference). (H+5 by default)


Loop, 1 {							;If SWTOR is not running it will launch the game once. Delete this if you prefer to launch the game and script manually.
IfWinNotExist, ahk_exe swtor.exe
Run, launcher.exe
WinWait, ahk_exe swtor.exe
}


Loop, {								;DO NOT TOUCH - Continuous loop to see if a cutscene is active to toggle off MouseLook mode.
If (MouseLook)
	{
	StructSize := A_PtrSize + 16
	VarSetCapacity(InfoStruct, StructSize)
	NumPut(StructSize, InfoStruct)
	DllCall("GetCursorInfo", UInt, &InfoStruct)
	Result := NumGet(InfoStruct, 8)
	If (Result)
		{
		MouseLook := 0
		SplashImage, Off
		Send {RButton Up}
		MouseMove, A_ScreenWidth, A_ScreenHeight, 0
		}
	}
Sleep, 1000
IfWinNotExist, ahk_exe swtor.exe
ExitApp
}


;;;;;;;;;; MouseLook Toggle Button ;;;;;;;;;;

`::									;MouseLook Toggle keybind. Adjust to your preference. (Tilda key (~) by default)
If (MouseLook == 0)
	{
	BlockInput, On
	MouseLook := 1
	MouseMove, W, H, 0
	Send {RButton Down}
	SplashImage, Crosshair.png, x%CHW% y%CHH% b,,,Crs
	WinSet, TransColor,White,Crs
	BlockInput, Off
	Sleep, 250
	Return
	}
MouseLook := 0
SplashImage, Off
Send {RButton Up}
Sleep, 250
Return


;;;;;;;;;; Mouse Button Keybinds during MouseLook ;;;;;;;;;;

#If (MouseLook)
{
	; Use ( loot, interact )
	
	*f::
		SetMouseDelay, 0
		Send {RButton Up}{Click}{RButton Down}
		Sleep, 100
		Send {RButton Up}{Click, Right}{RButton Down}
		SetMouseDelay, 10
		BlockInput, Off
	return
	
	
	; Mouse buttons
	
	*LButton::
		send {NumpadClear}
		KeyWait, LButton, T0.15
		if (ErrorLevel) {
			send ^3
			sleep, 10
			send ^2
		} else {
			send ^1
			sleep, 10
			send ^2
		}
	return

	*RButton::
		send {NumpadClear}
		KeyWait, RButton, T0.15
		if (ErrorLevel) {
			send ^6
			sleep, 10
			send ^2
		} else {
			send ^4
			sleep, 10
			send ^5
			sleep, 10
			send ^2
		}
	return
	
	
	; Action keys
	
	*tab::
		send ^tab
		sleep, 10
		send tab
	return
	
	*q::
		send ^q
		sleep, 10
		send q
	return
	
	*e::
		send ^e
		sleep, 10
		send e
	return
	
	*r::
		send ^r
		sleep, 10
		send r
	return
	
	*c::
		send ^c
		sleep, 10
		send c
	return
	
	*x::
		send ^x
		sleep, 10
		send x
	return
	
	
	; Buffs Alt + [1-4]
	
	*g::
		send !1
		sleep, 10
		send !2
		sleep, 10
		send !3
		sleep, 10
		send !4
	return
}


;;;;;;;;;; Interface Keys that toggle off MouseLook ;;;;;;;;;;

							;These are a list of default interface keybinds that will automatically toggle OFF MouseLook mode. Add more if you prefer. Use the format "~KEY::".

~L::
~U::
~O::
~Y::
~H::
~I::
~K::
~P::
~B::
~N::
~/::
~^P::
~^/::
~!Tab::
~LWin::
~RWin::
~Enter::
~Esc::
~Backspace::
~+Backspace::
If (MouseLook)
	{
	MouseLook := 0
	SplashImage, Off
	Send {RButton Up}
	Return
	}
Return