SWTOR MouseLook Combat mode 1.0 - created by Wayleran 11/14/2015


To use this script you of course need to download the freeware AutoHotkey from:

https://autohotkey.com/

Be sure to select "Run this program as an administrator" under ...Properties/Compatibility

Then simply extract "SWTOR MouseLook.ahk" and "Crosshair.png" into your main
Star Wars - The Old Republic folder (make sure it's in the same location as launcher.exe)
and run SWTOR MouseLook.ahk.

It will start the script AND launch the game for you. Also it will automatically close
the AutoHotkey script when you exit the game.

In order for the "Crosshair" reticle to appear in game you need to adjust the Graphics
settings to Fullscreen(Windowed).


The default setup for the MouseLook combat script is as follows:

The tilda ("~") key (to the left of the 1 key on your keyboard) will TOGGLE the script on/off.

HOLDING DOWN the Right Mouse Button while in MouseLook will send a right click at the reticle
location in essence allowing you to loot/interact/talk to NPC/etc... without having to toggle
out of MouseLook mode.

Also if you select an item/NPC that starts a cutscene the script will automatically toggle you
out of MouseLook mode.

And using most Interface keys will toggle off MouseLook when appropriate. (ie. Pressing "I" to
open your inventory will toggle MouseLook off.


Then while in MouseLook mode...

Left Mouse Button - 1

Right Mouse Button - 2

Scroll Wheel Up - 3

Scroll Wheel Down - 4

Middle Button Click - 5

Mouse Button 4 -or- the "Back Thumb" Button (if you have one) - 6

Mouse Button 5 -or- the "Forward Thumb" Button (if you have one) - UNSPECIFIED

...these will work the same with MODIFIERS (Shift, Ctrl, Alt...) so holding Shift and
clicking the Left Mouse Button for instance will use your Shift+1 action button.


If you choose to edit the keybinds and/or the keys to toggle MouseLook off simply right click
the SWTOR MouseLook.ahk file and open with notepad and edit the settings to your preferences.
For more info go to:

https://autohotkey.com/docs/KeyList.htm -or- AutoHotkey.com.


EA's stance on this isn't a problem because this script does not "automate" anything for you
allowing the script to perform actions while you are "AFK". It also doesn't alter any of the
game client files either.  It simply simulates holding the Right Mouse button down and rebinds
your mouse buttons to 1-6 on your action bar.