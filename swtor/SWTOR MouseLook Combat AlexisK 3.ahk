;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;; SWTOR MouseLook Combat mode 1.0 - created by Wayleran 11/14/2015 ;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

#NoEnv
#SingleInstance Force
DetectHiddenWindows, on
#IfWinActive ahk_exe swtor.exe


;;;;;;;;;; Variables ;;;;;;;;;;

MouseLook := 0						;DO NOT TOUCH - Sets MouseLook mode off initially.
IsRightMB := 0

W   := A_ScreenWidth/2				;Sets cursor's X coordinate. By default it's directly in the center of your screen. I'd recommend not adjusting this. (A_ScreenWidth/2 by default)
H   := A_ScreenHeight/2.5			;Sets cursor's Y coordinate. Change the 2.5 up or down to your preferred cursor height. (A_ScreenHeight/2.5 by default)

CHW := W							;Offsets the Crosshair horizontally (+/- to adjust to your preference). (Just W by default)
CHH := H+5							;Offsets the Crosshair vertically (+/- to adjust to your preference). (H+5 by default)


;;;;;;;;;; Functions ;;;;;;;;;;


MouseLookToggleOff() {
	global MouseLook, IsRightMB
	if ( MouseLook ) {
		MouseLook := 0
		SplashImage, Off
		if ( IsRightMB ) {
			Send {RButton Up}
		} else {
			Send {LButton Up}
		}
		IsRightMB := 0
		MouseMove, A_ScreenWidth, A_ScreenHeight, 0
	}
}

MouseLookToggleOn() {
	global MouseLook, IsRightMB, W, H, CHW, CHH
	if ( !MouseLook ) {
		BlockInput, On
		MouseLook := 1
		MouseMove, W, H, 0
		Send {LButton Down}
		SplashImage, Crosshair.png, x%CHW% y%CHH% b,,,Crs
		WinSet, TransColor,White,Crs
		BlockInput, Off
	}
}

ProcessMouseMode(b:=0) {
	global MouseLook, IsRightMB
	if ( MouseLook ) {
		if ( b && !IsRightMB ) {
			BlockInput, On
			IsRightMB := 1
			SetMouseDelay, 0
			Send {RButton Down}
			Send {LButton Up}
			SetMouseDelay, 10
			BlockInput, Off
		}
		if ( !b && IsRightMB ) {
			BlockInput, On
			IsRightMB := 0
			SetMouseDelay, 0
			Send {LButton Down}
			Send {RButton Up}
			SetMouseDelay, 10
			BlockInput, Off
		}
	}
}

Interact() {
	SetMouseDelay, 0
	if ( IsRightMB ) {
		send {RButton Up}{Click}{RButton Down}
	} else {
		send {LButton Up}{Click}{LButton Down}
	}
	Sleep, 100
	if ( IsRightMB ) {
		send {RButton Up}{Click, Right}{RButton Down}
	} else {
		send {LButton Up}{Click, Right}{LButton Down}
	}
	SetMouseDelay, 10
	BlockInput, Off
}

AttackEnd() {
	sleep, 10
	send ^2
	sleep, 10
	send ^1
}

;;;;;;;;;; Logic ;;;;;;;;;;

Loop, {	;DO NOT TOUCH - Continuous loop to see if a cutscene is active to toggle off MouseLook mode.
	If (MouseLook) {
		StructSize := A_PtrSize + 16
		VarSetCapacity(InfoStruct, StructSize)
		NumPut(StructSize, InfoStruct)
		DllCall("GetCursorInfo", UInt, &InfoStruct)
		Result := NumGet(InfoStruct, 8)
		If (Result) {
			MouseLookToggleOff()
		}
	}
	Sleep, 1000
	IfWinNotExist, ahk_exe swtor.exe
	ExitApp
}



`::	;MouseLook Toggle keybind
	If (MouseLook == 0) {
		MouseLookToggleOn()
	} else {
		MouseLookToggleOff()
	}
	Sleep, 250
Return



#If (MouseLook)
{
	
	~w::
	~s::
	~d::
	~a::
		ProcessMouseMode(1)
	return
	~w up::
	~s up::
		ProcessMouseMode(0)
	return

	*f::
		ProcessMouseMode()
		Interact()
	return
	
	
	*LButton::
		send {NumpadClear}
		KeyWait, LButton, T0.15
		if (ErrorLevel) {
			send ^4
			sleep, 10
			send ^3
			AttackEnd()
		} else {
			send ^3
			AttackEnd()
		}
	return

	*RButton::
		send {NumpadClear}
		KeyWait, RButton, T0.15
		if (ErrorLevel) {
			send ^6
			sleep, 10
			send ^5
			AttackEnd()
		} else {
			send ^5
			AttackEnd()
		}
	return
	
	
	; Action keys
	
	*tab::
		send ^tab
		sleep, 10
		send tab
	return
	
	*q::
		send ^q
		sleep, 10
		send q
		AttackEnd()
	return
	
	*e::
		send ^e
		sleep, 10
		send e
		AttackEnd()
	return
	
	*r::
		send ^r
		sleep, 10
		send r
		AttackEnd()
	return
	
	*c::
		send ^c
		sleep, 10
		send c
		AttackEnd()
	return
	
	*x::
		send ^x
		sleep, 10
		send x
		AttackEnd()
	return
	
	
	; Buffs Alt + [1-4]
	
	*g::
		send ^+1
		sleep, 10
		send ^+2
		sleep, 10
		send ^+3
		sleep, 10
		send ^+4
	return
}


;;;;;;;;;; Interface Keys that toggle off MouseLook ;;;;;;;;;;

							;These are a list of default interface keybinds that will automatically toggle OFF MouseLook mode. Add more if you prefer. Use the format "~KEY::".

~L::
~U::
~O::
~Y::
~H::
~*I::
~K::
~P::
~B::
~N::
~/::
~^P::
~^/::
~!Tab::
~LWin::
~RWin::
~Enter::
~Esc::
~Backspace::
~+Backspace::
MouseLookToggleOff()
Return
